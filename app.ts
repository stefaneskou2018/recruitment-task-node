require('dotenv').config();

import commentsRoutes from "./features/modules/comments/comments.router";
import moviesRoutes from './features/modules/movies/movies.router';
import * as express from 'express';

import * as bodyParser from "body-parser";


const app = express();

app.use(bodyParser.json());

app.use('/comments', commentsRoutes);
app.use('/movies', moviesRoutes);

app.listen(process.env.PORT, () => console.log(`App is listening on port ${process.env.PORT}`));

export default app;