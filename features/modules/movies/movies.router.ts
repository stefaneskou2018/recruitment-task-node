
import {Router} from 'express';
import {bindAll} from 'lodash';
import MoviesController from "./movies.controller";

const controller = new MoviesController();
const moviesRoutes = Router();

bindAll(controller, ['getMovies', 'createMovie']);

moviesRoutes.get('/', controller.getMovies);

moviesRoutes.post('/', controller.createMovie);


export default moviesRoutes;