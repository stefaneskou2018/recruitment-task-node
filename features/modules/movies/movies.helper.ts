class MoviesHelper {
    constructor(private dbURL: string, private dbSecret: string, private toJson: boolean) {}

    validateMovieBody(body: any): string {
        return (body && body.title) ? body.title : body;
    }

    buildOptions(movieTitle: string) {
        return {
            uri: this.dbURL,
            qs: {
                apikey: this.dbSecret,
                t: movieTitle,
                r: 'json'
            },
            json: this.toJson
        }
    }
}

export default new MoviesHelper( process.env.DB_MOVIE_URL, process.env.DB_MOVIE_SECRET, true);
