import db from '../../database';
import {isEqual} from 'lodash';
import {NextFunction, Request, Response} from "express";
import response from '../../utils/response';
import {IComment} from "../../models/comments/comments-interface";

export default class CommentsController {
    constructor() {}

    static commentsKeys: Array<string> = ['content', 'movieId'];

    getComment(req: Request, res: Response, next: NextFunction): void {
        const id: number = parseInt(req.params.id, 10);
        db.Models.Comment
            .findById(id)
            .then(response.send200.bind(this, res))
            .catch((err) => next(`Failed to fetch comment. Error message: ${err}`));
    }

    createComment(req: Request, res: Response, next: NextFunction): void {
        const comment: IComment = (req.body as IComment);
        if(!this.isCommentObjectValid(comment)) return next('Invalid comment object');
        db.Models.Comment
            .create(comment)
            .then(response.send200.bind(this, res))
            .catch((err) => next(`Failed to fetch comment. Error message: ${err}`));
    }

    private isCommentObjectValid(comment: IComment): boolean {
        const commentKeys: Array<string> = Object.keys(comment);
        return isEqual(commentKeys.sort(), CommentsController.commentsKeys.sort())
    }
}

