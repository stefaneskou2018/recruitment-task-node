
export interface IComment {
    id?: number;
    movieId: number;
    content: string;
}