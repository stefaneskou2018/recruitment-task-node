import {DataTypes, Instance, Sequelize} from 'sequelize';
import * as SequelizeStatic from 'sequelize';
import {IComment} from "./comments-interface";

export interface ICommentInstance extends Instance<IComment>, IComment {
}

export default function (sequelize: Sequelize, dataTypes: DataTypes): SequelizeStatic.Model<ICommentInstance, IComment> {
    return sequelize.define<ICommentInstance, IComment>('Comment', {
        movieId: {type: dataTypes.INTEGER},
        content: dataTypes.STRING
    }, {
        indexes: [],
        classMethods: {}
    });
}