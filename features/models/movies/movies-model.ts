import * as SequelizeStatic from 'sequelize';
import {IMovie} from "./movies-interface";
import {DataTypes, Instance, Sequelize} from "sequelize";

interface IMovieInstance extends Instance<IMovie>, IMovie {}

export default function (sequelize: Sequelize, dataTypes: DataTypes): SequelizeStatic.Model<IMovieInstance, IMovie> {
    return sequelize.define<IMovieInstance, IMovie>('Movie', {
            title: {type: dataTypes.STRING, allowNull: false},
            year: {type: dataTypes.INTEGER, allowNull: false},
            rated: {type: dataTypes.STRING, allowNull: false},
            released: {type: dataTypes.DATE, allowNull: false},
            runtime: {type: dataTypes.STRING, allowNull: false},
            genre: {type: dataTypes.STRING, allowNull: false},
            director: {type: dataTypes.STRING, allowNull: false},
            writer: {type: dataTypes.STRING, allowNull: false},
            actors: {type: dataTypes.STRING, allowNull: false},
            plot: {type: dataTypes.STRING, allowNull: false},
            language: {type: dataTypes.STRING, allowNull: false},
            country: {type: dataTypes.STRING, allowNull: false},
            awards: {type: dataTypes.STRING, allowNull: false},
            poster: {type: dataTypes.STRING, allowNull: false},
            ratings: {type: dataTypes.JSON, allowNull: false},
            metascore: {type: dataTypes.INTEGER, allowNull: false},
            imdbRating: {type: dataTypes.FLOAT, allowNull: false},
            imdbVotes: {type: dataTypes.STRING, allowNull: false},
            imdbID: {type: dataTypes.STRING, allowNull: false},
            type: {type: dataTypes.STRING, allowNull: false},
            dvd: {type: dataTypes.DATE, allowNull: false},
            boxOffice: {type: dataTypes.STRING, allowNull: false},
            production: {type: dataTypes.STRING, allowNull: false},
            website: {type: dataTypes.STRING, allowNull: false}
        },
        {
            indexes: [],
            classMethods:
                {}
        }
    )
        ;
}