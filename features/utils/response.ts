import {Response} from "express";

class ResponseUtil {
    constructor(){}

    send200 (res: Response, entityToSend: any) {
        return res
            .status(200)
            .json(entityToSend);
    }

    send500(res) {

    }

}


const response = new ResponseUtil();

export default response;