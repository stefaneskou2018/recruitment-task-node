import {mapKeys} from 'lodash';
import {IMovie} from "../models/movies/movies-interface";

export function lowerCaseFirstLetterOfKeys(movie: IMovie): IMovie {
    return (mapKeys((movie as any), function (val, key: string) {
        if(key === key.toUpperCase())
            key = key.toLowerCase();
        else
            key = key.substr(0, 1).toLowerCase() + key.substr(1);
        return key;
    }) as any);
}
