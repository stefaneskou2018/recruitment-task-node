# Intallation

### Make sure you already have those installed:

* Node.js ~ 6.10.X
* NPM (a version that comes with Node.js mentioned above)
* Docker
* Docker Compose
* Mocha installed globally (npm i mocha -g)

### How-to:

* Create .env file in root of a directory tree
    * It should have following variables:
    * ```PORT=
DB_MOVIE_URL=
DB_MOVIE_SECRET=
POSTGRES_USER=
POSTGRES_DB=
POSTGRES_PASSWORD=
POSTGRES_HOST=```
    * Where PORT is a port of node application, DB_MOVIE_URL should an url to open database, DB_MOVIE_SECRET should be your token and the rest are credentials to your database. Keep in mind that this file has to exist before you will launch docker.

* Start this command: ```npm i``` It is basically everything what you need to start this app!
